{
    local resources = import '../resources/main.jsonnet',
    local cnf = self,
    buildOperator(channel, opNs, opgrpName, targetNs, opName, opSubs)::
      [
         resources.buildNS(opNs),
         resources.buildOpGroup(opgrpName, opNs, targetNs),
         resources.buildSubs(opSubs, opNs, channel, opName)
      ],
    buildPAO(channel)::
      local opNs = "openshift-performance-addon";
      local opgrpName = "performance-addon-operatorgroup";
      local targetNs = opNs;
      local opName = "performance-addon-operator";
      local opSubs = "performance-addon-operator-subscription";
      cnf.buildOperator(channel, opNs, opgrpName, targetNs, opName, opSubs),
   buildPTP(channel)::
      local opNs = "openshift-ptp";
      local opgrpName = "ptp-operators";
      local targetNs = opNs;
      local opName = "ptp-operator";
      local opSubs = "ptp-operator-subscription";
      cnf.buildOperator(channel, opNs, opgrpName, targetNs, opName, opSubs),
   buildSRIOV(channel)::
      local opNs = "openshift-sriov-network-operator";
      local opgrpName = "sriov-network-operators";
      local targetNs = opNs;
      local opName = "sriov-network-operator";
      local opSubs = "sriov-network-operator-subscription";
      cnf.buildOperator(channel, opNs, opgrpName, targetNs, opName, opSubs),
}