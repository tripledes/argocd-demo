{
   buildNS(name)::
   {
      "apiVersion": "v1",
      "kind": "Namespace",
      "metadata": {
         "labels": {
            "openshift.io/cluster-monitoring": "true",
            "openshift.io/run-level": "1"
         },
         "name": name
      }
   },
   buildOpGroup(name, namespace, targetns)::
   {
      "apiVersion": "operators.coreos.com/v1",
      "kind": "OperatorGroup",
      "metadata": {
         "name": name,
         "namespace": namespace
      },
      "spec": {
         "targetNamespaces": [
            targetns
         ]
      }
   },
   buildSubs(name, namespace, channel, opname, opsource="redhat-operators")::
   {
      "apiVersion": "operators.coreos.com/v1alpha1",
      "kind": "Subscription",
      "metadata": {
         "name": name,
         "namespace": namespace
      },
      "spec": {
         "channel": channel,
         "name": opname,
         "source": opsource,
         "sourceNamespace": "openshift-marketplace"
      }
   },
}