local cnf = import 'lib/cnf/main.jsonnet';

function(channel="4.4")
    cnf.buildPAO(channel) +
    cnf.buildPTP(channel) +
    cnf.buildSRIOV(channel)